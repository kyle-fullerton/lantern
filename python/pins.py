
from enum import Enum, auto

class Pin(Enum):
    POWER_5V = auto()  # white
    GND = auto()  # black
    
    ENC_0 = auto()  # brown
    ENC_1 = auto()  # red
    ENC_2 = auto()  # orange
    ENC_3 = auto()  # yellow
    ENC_4 = auto()  # green
    ENC_5 = auto()  # blue
    ENC_6 = auto()  # purple
    ENC_7 = auto()  # gray
    
    LAMP_BTN = auto()  # white
    ALARM_BTN = auto()  # black
    TIME_BTN = auto()  # brown
    
    # TODO Add 7-segment control pins here

PHYSICAL_PINS = {
    Pin.POWER_5V: 4,
    Pin.GND: 6,
    Pin.ENC_0: 37,
    Pin.ENC_1: 35,
    Pin.ENC_2: 33,
    Pin.ENC_3: 31,
    Pin.ENC_4: 32,
    Pin.ENC_5: 36,
    Pin.ENC_6: 38,
    Pin.ENC_7: 40,
    Pin.LAMP_BTN: 11,
    Pin.ALARM_BTN: 13,
    Pin.TIME_BTN: 15
}

LOGICAL_PINS = {
    Pin.ENC_0: 26,
    Pin.ENC_1: 19,
    Pin.ENC_2: 13,
    Pin.ENC_3: 6,
    Pin.ENC_4: 12,
    Pin.ENC_5: 16,
    Pin.ENC_6: 20,
    Pin.ENC_7: 21,
    Pin.LAMP_BTN: 17,
    Pin.ALARM_BTN: 27,
    Pin.TIME_BTN: 22
}



import pins
from bourns_absolute_encoder import encode
from time import sleep


class AlarmKnob:
    def __init__(self):
        self.pins = list(pins.Pin.ENC_7,
                         pins.Pin.ENC_6,
                         pins.Pin.ENC_5,
                         pins.Pin.ENC_4,
                         pins.Pin.ENC_3,
                         pins.Pin.ENC_2,
                         pins.Pin.ENC_1,
                         pins.Pin.ENC_0)
        self.logical_pins = [pins.LOGICAL_PINS[pin] for pin in self.pins]

    def start(self):
        uhh set up all 8 listeners

    def read(self):
        values = [uhh read value of pin(logical_pin) for logical_pin in logical_pins]
        total = 0
        for v in values:
            total = total << 1
            if v:
                total += 1
        return encode(total=total)


if __name__ == "__main__":
    knob = AlarmKnob()
    callback = lambda(x): print("AlarmKnob read value of: {}".format(x))
    try:
        while True:
            callback(knob.read())
            sleep(0.5)
    except InterruptException:
        pass


